Este projeto é um projeto com terraform e docker simples
onde se conecta na AWS e cria uma EC2 que roda um comando docker, 
instalando um jogo do Mario que pode ser acessado

Para iniciar Basta criar um usuario AMI na AWS
e se conectar 
e rodar um comando terraform init dentro do diretorio devops/resources
e terraform apply

Acessar o ip IPv4 Publico gerado e Jogar

<img src="/img/mario.png">



